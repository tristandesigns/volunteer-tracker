package com.volunteertracker

import android.animation.AnimatorSet
import android.animation.LayoutTransition
import android.animation.ValueAnimator
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Typeface
import android.icu.util.Calendar
import android.os.Bundle
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.Gravity
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.*
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Reference and initialize the content layout + padding
        val linearlayout = findViewById<LinearLayout>(R.id.linearlayout)

        val topPadding = Space(this)
        topPadding.minimumHeight = 20
        linearlayout.addView(topPadding)

        // Entry class: items inside
        class entry(layout: LinearLayout, datestr: String,hoursint:Int) {
            val hoursints = hoursint
            val innerLayout = LinearLayout(this@MainActivity)
            val date = TextView(this@MainActivity)
            val hours = TextView(this@MainActivity)
            val textpadding = LinearLayout(this@MainActivity)

            init{
                Toast.makeText(applicationContext, "$datestr",Toast.LENGTH_SHORT).show()

                layout.addView(innerLayout)
                innerLayout.orientation = LinearLayout.HORIZONTAL
                innerLayout.setPadding(30,0,30,0)

                innerLayout.addView(date)
                date.setText(datestr)
                date.textSize = 20F
                date.setTypeface(date.typeface, Typeface.BOLD)
                date.setTextColor(Color.parseColor("#000000"))

                textpadding.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0,1F)
                innerLayout.addView(textpadding)
                innerLayout.addView(hours)
                hours.textSize = 20F
                hours.setTextColor(Color.parseColor("#000000"))
                hours.setTypeface(date.typeface, Typeface.BOLD)
                hours.setText("$hoursints Hours")
            }
        }


        // Category class: containers for Entry class above
        class category(description: String, allEnt: MutableMap<String,entry>) {
            var allEntries = allEnt
            var hourCount: Int = 0
            var text = SpannableString("${hourCount}\nhours")
            val cardview = CardView(this@MainActivity)
            val outerlayout = LinearLayout(this@MainActivity)
            val superouterlayout = LinearLayout(this@MainActivity)
            val innerlayout = LinearLayout(this@MainActivity)
            val date = TextView(this@MainActivity)
            val desc = TextView(this@MainActivity)
            val hours = TextView(this@MainActivity)
            val textpadding = View(this@MainActivity)
            val padding = Space(this@MainActivity)

            var isExpanded = false
            val height = 200
            val heightExpanded = 500
            val color = "#000000"

            var listOfEntries = setOf<String>()

            var buffer = 5
            val entryLayout = LinearLayout(this@MainActivity)

            init{
                linearlayout.addView(cardview)
                cardview.radius = 50.0F
                cardview.layoutParams = LinearLayout.LayoutParams(1020, height)
                cardview.addView(superouterlayout)
                innerlayout.orientation = LinearLayout.VERTICAL
                innerlayout.setPadding(30,15,30,15)
                outerlayout.orientation = LinearLayout.HORIZONTAL
                outerlayout.addView(innerlayout)
                outerlayout.setPadding(0,0,30,20)
                superouterlayout.orientation = LinearLayout.VERTICAL
                superouterlayout.addView(outerlayout)
                outerlayout.setBackgroundResource(R.drawable.bg_gradient)
                superouterlayout.setBackgroundResource(R.drawable.bg2_gradient)

                desc.text = description
                desc.textSize = 26F
                desc.setTypeface(date.typeface, Typeface.BOLD)
                desc.setTextColor(Color.parseColor(color))
                innerlayout.addView(desc)

                text.setSpan(RelativeSizeSpan(2f), 0,2, 0)
                hours.text = text
                hours.gravity = Gravity.END
                hours.textSize = 20F
                hours.setLineSpacing(0F,0.85F)
                hours.setTypeface(date.typeface, Typeface.BOLD)
                hours.setTextColor(Color.parseColor(color))

                textpadding.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0,1F)
                outerlayout.addView(textpadding)
                outerlayout.addView(hours)

                padding.minimumHeight = 20
                linearlayout.addView(padding)

                // Animation
                cardview.layoutTransition = LayoutTransition()
                fun animated(startHeight: Int, endHeight: Int) {
                    val anim = ValueAnimator()
                    anim.setIntValues(startHeight, endHeight)
                    anim.setDuration(500)
                    anim.addUpdateListener {
                        val value: Int = anim.animatedValue.toString().toInt()
                        cardview.layoutParams = LinearLayout.LayoutParams(cardview.layoutParams.width, value)
                    }
                    val animSet = AnimatorSet()
                    animSet.play(anim)
                    animSet.setInterpolator(AccelerateDecelerateInterpolator())
                    animSet.start()
                }

                // Creating a layout for the entries, under the category
                entryLayout.orientation = LinearLayout.VERTICAL
                superouterlayout.addView(entryLayout)

                // Click card expands/contracts
                cardview.setOnClickListener {
                    if (isExpanded) {
                        animated(cardview.layoutParams.height,height)
                    } else {
//                        if (!listOfEntries.isEmpty()) animated(cardview.layoutParams.height,cardview.layoutParams.height + (78 * listOfEntries.size))
                        if (!listOfEntries.isEmpty()) animated(cardview.layoutParams.height,cardview.layoutParams.height + buffer)
                    }
                    if (!listOfEntries.isEmpty()) isExpanded = !isExpanded
                }
            }

            fun updateCount(){
                for (i in listOfEntries) {
                    hourCount += allEntries.getValue(i).hoursints
                    text = SpannableString("${hourCount}\nhours")
                    text.setSpan(RelativeSizeSpan(2f), 0,2, 0)
                    hours.text = text
                }
            }
        }

        var allCats = mutableMapOf<String, category>()
        var allEnt = mutableMapOf<String, entry>()

        // Reference buttons
        val addCat = findViewById<FloatingActionButton>(R.id.addCat)
        val addEnt = findViewById<FloatingActionButton>(R.id.addEnt)

        // Button to create Categories
        addCat.setOnClickListener {

            // Set category description
            val builder = AlertDialog.Builder(this@MainActivity)
            builder.setTitle("Create: Category")
            val input = EditText(this@MainActivity)
            builder.setView(input)
            builder.setPositiveButton("Select", DialogInterface.OnClickListener { dialog, which ->
                var addText = input.getText().toString()
                allCats.put(addText,category(addText,allEnt))
            })
            builder.setNegativeButton("Cancel",DialogInterface.OnClickListener { dialog, which ->
                dialog.cancel()
            })
            builder.show()
        }

        // Button to create Entries
        addEnt.setOnClickListener {

            // Date Picker
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            var date = ""
            val datepick = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val monthsRef = listOf<String>("January","February","March","April","May","June","July","August","September","October","November","December")
                date = "${monthsRef.get(monthOfYear)} ${dayOfMonth}, $year"

                // Hour Picker
                val picker = NumberPicker(this)
                picker.minValue = 1
                picker.maxValue = 24
                val pickerLayout = LinearLayout(this)
                pickerLayout.orientation = LinearLayout.VERTICAL
                pickerLayout.addView(picker)
                val pickerBuild = AlertDialog.Builder(this)
                pickerBuild.setTitle("Create Entry: Select Hour Count")
                pickerBuild.setView(pickerLayout)
                pickerBuild.setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, which ->
                    val hours = picker.value

                    // Category Picker
                    val builder = AlertDialog.Builder(this@MainActivity)
                    builder.setTitle("Create Entry: Select Category")
                    var listOfCats = allCats.keys.toTypedArray()

                    builder.setSingleChoiceItems(listOfCats, -1) { dialog, which ->
                        // Create Entries
                        allEnt.put(listOfCats[which],entry(allCats.getValue(listOfCats[which]).entryLayout,date,hours))
                        allCats.getValue(listOfCats[which]).listOfEntries += listOfCats[which]
                        allCats.getValue(listOfCats[which]).buffer += 70
                        allCats.getValue(listOfCats[which]).updateCount()
                        dialog.dismiss()
                    }
                    builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                        dialog.cancel()
                    })
                    builder.show()

                    dialog.dismiss()
                })
                pickerBuild.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                    dialog.cancel()
                })
                pickerBuild.show()
            }, year, month, day)
            datepick.show()


        }


    }
}
